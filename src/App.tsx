import React from "react";
import { appStore } from "./store/app-store";
import { observer } from "mobx-react";

const App = observer(() => {
  if (appStore.error) {
    return (
      <div>
        <span>Error!</span>
      </div>
    );
  }
  return (
    <div className='App'>
      <h1>api is working !</h1>
      {appStore.items &&
        appStore.items.map((el: any) => {
          return (
            <div key={el.email}>
              <span>{el.id}</span>
            </div>
          );
        })}
    </div>
  );
});

export default App;

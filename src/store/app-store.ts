import axios from "axios";
import { makeAutoObservable, runInAction } from "mobx";

const BASE_URL =
  "http://www.filltext.com/?rows=32&id=%7Bnumber%7C1000%7D&firstName=%7BfirstName%7D&lastName=%7BlastName%7D&email=%7Bemail%7D&phone=%7Bphone%7C(xxx)xxx-xx-xx%7D&address=%7BaddressObject%7D&description=%7Blorem%7C32%7D";

interface itemType {
  address: any;
  description: string;
  email: string;
  firstName: string;
  id: number;
  lastName: string;
  phone: string;
}

class Store {
  items: itemType[] = [];
  error: boolean = false;

  constructor() {
    makeAutoObservable(this);
  }

  getItems = (items: itemType[]) => {
    this.items = items;
  };
  throwError = () => (this.error = true);

  fetchData = runInAction(async () => {
    try {
      const res: { data: itemType[] } = await axios.get(BASE_URL);
      this.getItems(res.data);
    } catch {
      this.throwError();
    }
  });
}

export const appStore = new Store();
